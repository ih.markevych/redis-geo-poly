local _NAME = 'geo.lua'
local _VERSION = '0.0.1'
local _DESCRIPTION = 'A helper library for Redis geospatial indices'
local _COPYRIGHT = '2020 Oleksiy Babich, Ihor Markevych'

local Geo = {}      -- GEO library

-- private

-- Geospatial 2D Geometry
local Geometry = {}
Geometry.__index = Geometry

setmetatable(Geometry, {
              __call = function(cls, ...)
                return cls.new(...)
                end })

Geometry._TYPES = { 'Point',           -- TODO
                    'MultiPoint',      -- TODO
                    'LineString',      -- TODO
                    'MultiLineString', -- TODO
                    'Polygon',
                    'MultiPolygon' }   -- TODO
Geometry._TENUM = {}
for i = 1, #Geometry._TYPES do
  Geometry._TENUM[Geometry._TYPES[i]:upper()] = i
  Geometry['_T' .. Geometry._TYPES[i]:upper()] = i
end

--- Calculates distance between two coordinates.
-- Just like calling GEODIST, but slower and of slightly different
-- accuracy.
-- @param lon1 The longitude of the 1st coordinate
-- @param lat1 The latitude of the 1st coordinate
-- @param lon2 The longitude of the 2nd coordinate
-- @param lat2 The latitude of the 2nd coordinate
-- @return distance The distance in meters
Geometry._distance = function (lon1, lat1, lon2, lat2)
  local R = 6372797.560856 -- Earth's, in meters
  local lon1r, lat1r, lon2r, lat2r =
    math.rad(lon1), math.rad(lat1), math.rad(lon2), math.rad(lat2)
  local u = math.sin((lat2r - lat1r) / 2)
  local v = math.sin((lon2r - lon1r) / 2)
  return 2.0 * R * math.asin(math.sqrt(u * u + math.cos(lat1r) * math.cos(lat2r) * v * v))
end

--- Geometry constructor.
-- Creates a geometry object.
-- @param geomtype The geometry's type, optional
-- @param ... The geometry's geometries
-- @return self New Geometry object
Geometry.new = function(geomtype, ...)
  local self = setmetatable({}, Geometry)
  self.geomtype = geomtype
  self.coordn, self.coordx, self.coordy = {}, {}, {}
  self.meta = {}

  if #arg > 0 then
    if self.geomtype == Geometry._TPOLYGON then
      -- A polygon's coordinates are given by one or more LineRings
      -- A LineRing is a LineString where the first and last vertices are identical
      -- The first and mandatory ring is the polygon's outer ring
      -- Any subsequent rings are considered holes, islands, and repeating...

      -- Polygon vertices are stored as two coordinate lists
      -- When there's more than one ring, O means the (0,0) and the two lists:
      --   begins with O
      --   rings are delimeted with O
      --   end with O
      self.coordx[#self.coordx+1], self.coordy[#self.coordy+1] = 0, 0
      for _, ring in ipairs(arg[1]) do
        local n = #ring / 2
        self.coordn[#self.coordn+1] = n
        for i = 1, n do
          self.coordx[#self.coordx+1], self.coordy[#self.coordy+1] = ring[2*i-1], ring[2*i]
        end
        self.coordx[#self.coordx+1], self.coordy[#self.coordy+1] = 0, 0
      end

      -- the outer ring
      local op = 0                    -- outer perimeter
      local ov = self.coordn[1]       -- number of vertices
      local bbx1 = self.coordx[2]     -- bounding box
      local bby1 = self.coordy[2]
      local bbx2 = self.coordx[2]
      local bby2 = self.coordy[2]
      local bbr = 0
      local bcx, bcy, bcr = 0, 0, 0   -- bounding circle
      -- traverse outer vertices
      for i = 2, 1 + ov do
        local ix, iy = self.coordx[i], self.coordy[i]
        local jx, jy = self.coordx[i+1], self.coordy[i+1]
        -- grow the outer perimeter
        op = op + Geometry._distance(ix, iy, jx, jy)
        -- add up for the averages
        bcx = bcx + ix
        bcy = bcy + iy
        -- adjust bounding box if needed
        if bbx1 > ix then
          bbx1 = ix
        elseif bbx2 < ix then
          bbx2 = ix
        end
        if bby1 > iy then
          bby1 = iy
        elseif bby2 < iy then
          bby2 = iy
        end
      end
      bcx = bcx / (ov - 1)
      bcy = bcy / (ov - 1)
      bbr = Geometry._distance(bbx1, bby1, bbx2, bby2) / 2

      -- farthest point from the centroid is the radius
      for i = 2, 1 + ov do
        local d = Geometry._distance(bcx, bcy, self.coordx[i], self.coordy[i])
        if bcr < d then
          bcr = d
        end
      end

      -- lets not be too accurate
      bcr = math.ceil(bcr)
      bbr = math.ceil(bbr)

      self.meta = { op, ov, bcx, bcy, bcr, bbx1, bby1, bbx2, bby2, bbr }
    else
      error('Unknown argument(s) to for type')
    end
  end

  return self
end


--- Checks if a point is inside a polygon.
-- https://www.ecse.rpi.edu/~wrf/Research/Short_Notes/pnpoly.html
-- @param pt Point
-- @param po Polygon
-- @return boolean True if PIP
function Geometry:PNPOLY(x, y)
  local c = false
  -- don't think outside the box

  if self.meta[6]<=x and self.meta[8]>=x and self.meta[7]<=y and self.meta[9]>=y then
    local nvert = #self.coordy
    local vertx = self.coordx
    local verty = self.coordy
    local testx = x
    local testy = y
    local j = nvert

    for i = 1, nvert do
      if  ((verty[i]>testy) ~= (verty[j]>testy)) and
          testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i] then
        c = not c
      end
      j = i
    end
  end

  return c
end

--- Returns the bounding circle for a geometry.
-- @return Table three elements: x, y and radius
function Geometry:getBoundingCircle()
  -- TODO: error on `Point`
  return { self.meta[3], self.meta[4], self.meta[5] }
end

function Geometry:getIndexPoints()
  local points = {}

  local bbx1 = math.floor(self.meta[6])
  local bby1 = math.floor(self.meta[7])
  local bbx2 = math.ceil(self.meta[8])
  local bby2 = math.ceil(self.meta[9])

  for x = bbx1, bbx2 do
    for y = bby1, bby2 do
      points[#points+1] = tostring(x)..":"..tostring(y)
    end
  end

  return points
end

--- Returns the bounding box for a geometry.
-- return Table five elements: min x and y, max x and y, radius
function Geometry:getBoundingBox()
  -- TODO: error on `Point`
  return { self.meta[6], self.meta[7], self.meta[8], self.meta[9], self.meta[10] }
end

--- Returns the geometry's type name.
-- @return name The geometry's name
function Geometry:typeAsString()
  return Geometry._TYPES[self.geomtype]
end


--- Returns the msgpack-serialized geometry object.
-- @return string The serialized object
function Geometry:dump()
  local payload = {}
  payload[#payload+1] = self.geomtype
  payload[#payload+1] = self.coordn
  payload[#payload+1] = self.coordx
  payload[#payload+1] = self.coordy
  payload[#payload+1] = self.meta
  return cmsgpack.pack(payload)
end

--- Loads the geometry from its serialized form.
-- @param msgpack The serialized geometry
-- @return null
function Geometry:load(msgpack)
  local payload = cmsgpack.unpack(msgpack)
  self.geomtype = table.remove(payload, 1)
  self.coordn = table.remove(payload, 1)
  self.coordx = table.remove(payload, 1)
  self.coordy = table.remove(payload, 1)
  self.meta = table.remove(payload, 1)
end

-- Data structure type
Geo._TYPE_GEO = 1     -- regular geoset
Geo._TYPE_XYZ = 2     -- xyzset
Geo._TYPE_GEOM = 3    -- geomash

--- Keys validation.
-- Extract and validate types of keys for command
-- @param geotype The type of command
-- @return geoset|polyhash Key name
-- @return [azset]  Key name
Geo._getcommandkeys = function (geotype)

  local function asserttype(k, t)
    local r = redis.call('TYPE', k)
    assert(r['ok'] == t or r['ok'] == 'none', 'WRONGTYPE Operation against a key holding the wrong kind of value')
  end

  if geotype == Geo._TYPE_GEO then
    local geokey = assert(table.remove(KEYS, 1), 'No geoset key name provided')
    asserttype(geokey, 'zset')
    return geokey
  elseif geotype == Geo._TYPE_XYZ then
    local geokey = assert(table.remove(KEYS, 1), 'No geoset key name provided')
    asserttype(geokey, 'zset')
    local zsetkey = assert(table.remove(KEYS, 1), 'No altitude sorted set key name provided')
    asserttype(zsetkey, 'zset')
    return geokey, zsetkey
  elseif geotype == Geo._TYPE_GEOM then
    local geomkey = assert(table.remove(KEYS, 1), 'No geomash key name provided')
    asserttype(geomkey, 'hash')
    return geomkey
  end
end

-- public API

--- Calculates the length of a path given by members.
-- @return length Path's length in meters
Geo.GEOPATHLEN = function()
  local geokey = Geo._getcommandkeys(Geo._TYPE_GEO)
  assert(#ARGV > 1, 'Need at least two members to make a path')

  local total = 0
  local prev = table.remove(ARGV, 1)
  while #ARGV > 0 do
    local curr = table.remove(ARGV, 1)
    local dist = redis.call('GEODIST', geokey, prev, curr, 'm')
    if dist then
      total = total + dist
      prev = curr
    else
      return
    end
  end

  return total
end

--- Upserts a single geometry into a geomash.
-- TODO: support more geometries besides an unholey `Polygon`
-- TODO: support upsert of multiple geometries
-- @return upserted Number 0 if updated, 1 if added
Geo.GEOMETRYADD = function()
  local geomkey = Geo._getcommandkeys(Geo._TYPE_GEOM)
  local geomtype = assert(table.remove(ARGV, 1), 'Expecting a geometry type')
  geomtype = geomtype:upper()
  geomtype = assert(Geometry._TENUM[geomtype], 'Invalid geometry type')
  local id = assert(table.remove(ARGV, 1), 'Expecting a geometry id')

  assert(geomtype == Geometry._TPOLYGON, 'Sorry, atm only `POLYGON` geometry type is supported')
  assert(#ARGV > 7, 'Expecting at least 4 coordinates')
  assert(#ARGV % 2 == 0, 'Expecting an even number of arguments as coordinates')
  assert(ARGV[1] == ARGV[#ARGV-1] and ARGV[2] == ARGV[#ARGV], 'The first and last vertices must be the identical')

  local coord = {}
  for i, v in ipairs(ARGV) do
    coord[#coord+1] = assert(tonumber(v), 'Expecting numbers as coordinates')
  end

  local geom = Geometry.new(Geometry._TPOLYGON, { coord })

  -- adding index
  redis.debug(geom:getIndexPoints())
  for i, point in ipairs(geom:getIndexPoints()) do
    redis.call('SADD', point, id)
  end

  return redis.call('HSET', geomkey, id, geom:dump())
end

--- Performs a search for all polygons for which point that is set with x y coordinates is inside.
-- @return Table with the polygons
Geo.FINDPOLYS = function()
  local geomkey = Geo._getcommandkeys(Geo._TYPE_GEOM)

  local x_point = tonumber(ARGV[1])
  local y_point = tonumber(ARGV[2])
  local reply = {}
  local polys = redis.call("HGETALL", geomkey)
  for i, pol in ipairs(polys) do
    if i % 2 == 0 then
      local geom = Geometry:new()
      -- local r = redis.call('HGET', geomkey, pol)
      geom:load(pol)
      if geom:PNPOLY(x_point, y_point) then
        reply[#reply+1] = polys[i-1]
      end
    end
  end

  return reply
end

Geo.FINDPOLYSI = function()
  local geomkey = Geo._getcommandkeys(Geo._TYPE_GEOM)

  local x_point = tonumber(ARGV[1])
  local y_point = tonumber(ARGV[2])

  local bx1 = math.floor(x_point)
  local bx2 = math.floor(x_point)
  local by1 = math.ceil(y_point)
  local by2 = math.ceil(y_point)

  local keys = redis.call('SUNION', unpack({ bx1..":"..by1, bx2..":"..by1, bx1..":"..by2, bx2..":"..by2}))
  local reply = {}

  if next(keys) ~= nil then
    local polys = redis.call("HMGET", geomkey, unpack(keys))

    for i, pol in ipairs(polys) do
      local geom = Geometry:new()
      -- local r = redis.call('HGET', geomkey, pol)
      geom:load(pol)
      if geom:PNPOLY(x_point, y_point) then
        reply[#reply+1] = keys[i]
      end
    end
  end

  return reply
end


-- "main"
assert(redis.call('COMMAND', 'INFO', 'GEOADD'), 'Redis GEO API is missing (are you using v3.2 or above?)')

local command_name = assert(table.remove(ARGV, 1), 'No command provided - try `help`')
command_name = command_name:upper()

local command = assert(Geo[command_name], 'Unknown command ' .. command_name)
return command()
