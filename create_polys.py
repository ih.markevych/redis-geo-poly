import geopandas as gpd
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
from tqdm import tqdm
import redis
from itertools import chain
from random import random

data_exp = gpd.read_file("./exploded.shp")

red = redis.StrictRedis()
pipe = red.pipeline()
with open('./geo.lua', 'r') as file:
    script = red.register_script(file.read())

for i, row in tqdm(enumerate(data_exp.geometry[:])):
    for k in range(1, 30):
        sx = random() - 0.5
        sy = random() - 0.5
        xy = list(zip([c + sx for c in list(row.exterior.coords.xy[0])],
                  [c + sy for c in list(row.exterior.coords.xy[1])]))
        args = ['GEOMETRYADD', 'polygon', 'p' + str(i) + 'k' + str(k)] + list(chain.from_iterable(xy))
        script(args=args, keys=['poly_zips'], client=pipe)
pipe.execute()