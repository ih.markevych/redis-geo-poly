# Redis GEO Polygon search

Based on https://github.com/RedisLabs/geo.lua

## Usage

### Load data to Redis

You can use `python` script here in `create_polys.py` to load any shape data
For ex., https://www.census.gov/geographies/mapping-files/2018/geo/carto-boundary-file.html

Or any other polygons, like stored datasets in `geopandas`, etc.

### Load script

To load script specifically in Redis CLI you can do

```sh
$ redis-cli SCRIPT LOAD "$(cat geo.lua)"
"XX"
$ redis-cli EVALSHA XX 1 poly_zips FINDPOLYSI -81.282078 28.564349
```

Example above shows that we run function `FINDPOLYSI` on set of polygons `poly_zips` to find all polygons which cover point with coords `(-81.282078 28.564349)`

## Use case

Generally speaking for single specific 2D point we want to find all polygons that cover it

We assume that point is single, but we have millions of polygons

ElasticSearch calls this functionality "percolate" (aka reverse search) and really disappoints in performance

### Example

You want to know when restaurants will reopen in some area, or city, or county. Any of this is polygon. And restaurant is location point.

And there are millions of people like you who want to get notification about reopening.  And single person notification is defined by area (aka polygon).

As soon as restaurant have open information we need to find all people whose area covers this restaurant and notify.

You may imagine many more scenario where you may need to find multitudes of polygons covering single point.

## Implementation

### `FINDPOLYS`

This simple implementation just iterates all polygons and selects which covers the point. Performance improvement is minimal and only filters out by boundary box which is relatively fast check.

Not much upgrade from base `geo.lua` and intended mostly to get familiar with data types and command APIs.

Also, it sucks inperformance almost as much as ElasticSearch.

### `FINDPOLYSI`

Here we are making things more interesting:

1. We add new type of indexing to the Polygon object:

  Imagine matrix of all coordinates: from -180 to 180 and from -90 to 90

  You'll have about 65k of points and we build index around this

  For each `GEOMETRYADD` call we remember which points are covered by boundary box of our polygon (which is really simple and fast operation)

2. When we search for a point we take 4 points which define square around  our point and lookup for union of all polygons hovering this square (Redis `SET` operations)

When you do this really fast pre-filter you can do precise check for you spcific point which will have to iterate much smaller set of polygons.

This indexing approach is signigicantly better in terms of speed.


### Future improvements

Of course, index costs memory. But further speed improvements may be reached by increasing index precision (from 1 degree to 0.1 or 0.01) in cost of even more memory.

Other indexing improvements for really lightning pre-filter may be based on use-cases and having/saving more information about areas, not only coordinates of polygonal points.

What's more, you can elimiante second precision filtering if your pre-filter is good enough and it's results will satisfy use case.

# License

MIT

# Authors
[Oleksiy Babich](mailto:oleksiy@oleksiy.od.ua) and [Ihor Markevych](mailto:ih.markevych@gmail.com)
